#!/bin/bash
#
# This script to delete or disable user accounts and archive their home directory.
#

ARCHIVE_DIR='/archive'

checkroot () {
    if [[ ${UID} -ne 0 ]]; then
        echo "Command must be run as root." >&2
        exit 1
    fi
}

show_usage () {
    echo "Usage: ${0} [-dra] USER [...USER] " >&2
    echo "Disable or delete a user account." >&2
    echo "  -d          Delete account instead of disabling it." >&2
    echo "  -r          Remove home directory" >&2
    echo "  -a          Create archive in /archives" >&2
    exit 1
}

disable_user () {
    chage -E 1 ${USERNAME}

    if [[ "${?}" -ne 0 ]]; then
      echo "Account ${USERNAME} was NOT disabled." >&2
      exit 1
    fi

    echo "Disabled account of ${USERNAME}."
}

delete_user () {
    if [[ ${REMOVE_HOME} = 'true' ]]; then
        echo "Removing ${USERNAME} including home dir..."
        userdel -r ${USERNAME}
    else
        echo "Removing ${USERNAME} (leaving) home dir..."
        userdel ${USERNAME}
    fi

    # Make sure user got deleted
    if [[ "${?}" -ne 0 ]]; then
        echo "The account ${USERNAME} was NOT deleted" >&2
        exit 1
    fi

    echo "Account ${USERNAME} deleted."
}

archive_home_dir () {
    echo "Archiving home folder of ${USERNAME}..."

    # First check if home exists.
    mkdir -p ${ARCHIVE_DIR}
    if [[ "${?}" -ne 0 ]]; then
        echo "Could not create archive directory." >&2
        exit 1
    fi

    # Create tar archive of user folder and save it to archive.
    HOME_DIR=/home/${USERNAME}/
    FILE_NAME="${USERNAME}.tar.gz"
    if [[ -d ${HOME_DIR} ]]; then
        tar -zcf "${ARCHIVE_DIR}/${USERNAME}.tar.gz" ${HOME_DIR}
    else
        echo "User home folder does not exist. Nothing to archive."
        return
    fi

    if [[ "${?}" -ne 0 ]]; then
        echo "Could not create user archive file" >&2
        exit 1
    fi
    echo "Compressed and moved user folder to ${ARCHIVE_DIR}/${FILE_NAME}"
}

# Enforces that it's executed as superuser or exit 1
checkroot

while getopts dra OPTION
do
  case ${OPTION} in
    d) DELETE='true' ;;
    r) REMOVE_HOME='true' ;;
    a) ARCHIVE_HOME='true' ;;
    ?) show_usage ;;
  esac
done

# Remove the options while leaving the remaining arguments]
shift "$(( OPTIND -1 ))"

if [[ "${#}" -le 0 ]]; then
  show_usage
fi

# For each remaing argument, assuming thats a username, execute the actual script.
for USERNAME in "${@}"; do
    shift

    # Check if UID is > 1000:
    USER_ID=$(id -u ${USERNAME})
    echo ${USER_ID}${USERNAME}
    if [[ "${USER_ID}" = "" ]]; then
        echo "User ${USERNAME} does not seem to exit." >&2
        exit 1
    fi

    if [[ "${USER_ID}" -le 1000 ]]; then
        echo "You may not delete system users" >&2
        exit 1
    fi

    if [[ ${ARCHIVE_HOME} = 'true' ]]; then
        archive_home_dir
    fi

    if [[ ${DELETE} = 'true' ]]; then
        delete_user
    else
        disable_user
    fi
done