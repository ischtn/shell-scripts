#!/bin/bash

# Enforces that it's executed as superuser or exit 1
if [[ ${UID} -ne 0 ]]; then
  echo 'Command must be run as root.'
  exit 1
fi

# Prompts username and initial password
# Ask for username.
read -p 'Enter username to create: ' USER_NAME

# Ask for real name.
read -p 'Enter name of person who this account is for: ' COMMENT

# Ask for the password
read -p 'Enter password to use for the account: ' PASSWORD

# Create new user
useradd -c "${COMMENT}" -m ${USER_NAME}

if [[ "${?}" -ne 0 ]]; then
  echo 'User could not be created'
  exit 1
fi

# Set password
echo ${PASSWORD} | passwd --stdin ${USER_NAME}

# Force password change on first login
passwd -e ${USER_NAME}

# Reply created or exit 1
if [[ "${?}" -ne 0 ]]; then
  echo 'Password cound not be reset. Creation failed'
  exit 1
fi

# Display username password and host
echo "User created ${USER_NAME} with password ${PASSWORD} on ${HOSTNAME}"
