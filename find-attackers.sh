#!/bin/bash

FILENAME=${1}
LIMIT='10'

# Show info if no log file given
if [[ "${#}" -eq 0 ]]; then
  echo "Provide a filename." >&2
  exit 1
fi

# Check that file exists
if [[ ! -e ${FILENAME}  ]]; then
  echo "File ${FILENAME} does not exist." >&2
  exit 1
fi

# Loop through items
echo 'Count,IP,Location'
grep "Failed" ${FILENAME} | awk -F 'from ' '{print $2}' | awk '{print $1}' | sort | uniq -c | sort -nr | while read COUNT IP; do
  if [[ "${COUNT} -gt ${LIMIT}" ]]; then
    LOCATION=$(geoiplookup ${IP} | awk -F ',' '{print $2}')
    echo "${COUNT},${IP},${LOCATION}"
  fi
done

