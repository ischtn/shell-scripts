#!/bin/bash

# Enforces that it's executed as superuser or exit 1
if [[ ${UID} -ne 0 ]]; then
  echo "Command must be run as root." >&2
  exit 1
fi

# Check usage
if [[ "${#}" -lt 1 ]]; then
  echo "Usage: $(basename ${0}) USERNAME COMMENT" >&2
  echo "Create an account on the local system with the name of USERNAME and a comments field of COMMENT" 2> /dev/null
  exit 1
fi

# First param is username
# Rest is comment
USER_NAME="${1}"
echo ${USER_NAME} &> /dev/null
shift

COMMENT="${*}"
echo ${COMMENT} &> /dev/null

#   if [[ i -eq 1 ]]; then
#     USER_NAME=${1}
#   fi
#   shift
# done

# Prompts username and initial password
# Ask for username.
# read -p 'Enter username to create: ' USER_NAME

# Ask for the password
# read -p 'Enter password to use for the account: ' PASSWORD

# Create new user
useradd -c "${COMMENT}" -m ${USER_NAME} &> /dev/null

if [[ "${?}" -ne 0 ]]; then
  echo 'User could not be created' >&2
  exit 1
fi

# Set password
PASSWORD=$(date +%s%N${RANDOM}${RANDOM} | sha256sum | head -c48)
echo ${PASSWORD} | passwd --stdin ${USER_NAME} &> /dev/null

# # Force password change on first login
passwd -e ${USER_NAME} &> /dev/null

# Reply created or exit 1
if [[ "${?}" -ne 0 ]]; then
  echo 'Password cound not be reset. Creation failed' >&2
  exit 1
fi

# Display username password and host
echo "User created ${USER_NAME}: ${COMMENT} with password: ${PASSWORD} on ${HOSTNAME}"
