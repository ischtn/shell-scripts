#!/bin/bash


# get type of a command
type -a command

# grep -v inverts criteria.
grep -v '^first,last$'


# awk
awk -F 'DATA:' '{print $2}' people.dat
# separator is 'DATA:'
# {} action is: print second field.'

# Display first and third field of passwd file.
cut -d ':' -f 1,3 /etc/passwd

# Similar in awk: (, means: spearate by default separator = space.)
awk -F ':' '{print $1,$3}' /etc/passwd

# OFS -> Output field separator
awk -F ':' -v OFS='--' '{print $1,$3}' /etc/passwd

# But, setting it manually works as well:
awk -F ':' '{print $1 ", " $3}' /etc/passwd

# Unlike cut, awk allows you to change the order of the printed columns.
awk -F ':' '{print $3 "," $1}' /etc/passwd

# Last field
awk -F ':' '{print $NF}' /etc/passwd

# Split by x number of spaces.
```
L1C1     L1C2
   L2C1   L2C2
        L3C1 L3C2
```

awk '{print $1, $2}' lines

netstat -nutl
# -n numbers instead of programs
# -u udp info
# -t tcp info
# -l for listening ports.
# -p to add pid

# Cut the two top rows off of the output to remove the header
netstat -nutl | grep -Ev '^Active|^Proto'
# Alternative, because all but the header lines have colons in them.
netstat -4nutl | grep ':'

# Only the ips with open ports
netstat -nutl | grep -Ev '^Active|^Proto' | awk '{print $4}'



# Just the open ports for ipv4 and ipv6:
netstat -nutl | grep -Ev '^Active|^Proto' | awk '{print $4}' | awk -F ':' '{print $NF}'

# Open ports for ipv4
netstat -4nutl | grep -Ev '^Active|^Proto' | awk '{print $4}' | awk -F ':' '{print $NF}'

netstat -4nutl | grep ':' | awk '{print $4}' | cut -d ':' -f 2

# Only unique items, numerically sorted.
netstat -nutl | grep ':' | awk '{print $4}' | awk -F ':' '{print $NF}' | sort -n -u

# Users, sorted by uid (numerical sort, with -n)
cut -d ':' -f 3 /etc/passwd | sort -n

# Who is using most space
sudo du /var/ | sort -n

# Human readable sizes, and human numeric sort.
sudo du -h /var/ | sort -h

sudo du -h /var/ | sort -h | uniq
# NOTE uniq only checks the item before it, so it requires a sorted list as input!

# uniq is still useful, i.e. to display number of occurences for each line:
netstat -nutl | grep ':' | awk '{print $4}' | awk -F ':' '{print $NF}' | sort -n | uniq -c

```
      2 22
      2 25
      1 68
      2 323
      1 14397
      1 19060
```

netstat -nutl


# Who is writing messages to syslog?
sudo cat /var/log/messages | awk '{print $5}' | sort | uniq -c | sort -n
# First sort by proces, then count occurences, then sort again, by the number of occurences.

# Useful alternative to count number of lines (i.e. of /etc/passwd)
wc /etc/passwd
# -w word count
# -c character count
# -l line count

# Count users login in with bash
cat /etc/passwd | grep '/bin/bash' | wc -l

# Grep can count simple stuff like this too with the -c flag!
grep -c bash /etc/passwd



# Sort keys
cat /etc/passwd | sort -t ':' -k 3 -n
# -t allows you to define the separator,
# -k allows setting the key (3rd column in this case).
# -n to sort numerically
