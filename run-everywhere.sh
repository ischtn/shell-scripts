#!/bin/bash

SERVER_LIST='/vagrant/servers'

SSH_OPTIONS='-o ConnectTimeout=2'

show_usage () {
    echo "Usage: ${0} [-nsv] [-f FILE] COMMAND " >&2
    echo "Run COMMAND as single command on every server." >&2
    echo "  -f FILE  Use FILE for the list of servers DEFAULT ${SERVER_LIST}." >&2
    echo "  -n       Dry run." >&2
    echo "  -s       Execute the COMMAND using sudo on remote server." >&2
    echo "  -v       Verbose. Displays server before running COMMAND" >&2
    exit 1
}

# Enforces that it's executed as superuser or exit 1
if [[ ${UID} -eq 0 ]]; then
  echo 'Do not use this command as root. Use the s option instead.'
  show_usage
  exit 1
fi

while getopts f:nsv OPTION ; do
  case ${OPTION} in
    f) SERVER_LIST="${OPTARG}" ;;
    n) DRY_RUN='true' ;;
    s) SUDO='sudo' ;;
    v) VERBOSE='true' ;;
    ?) show_usage ;;
  esac
done

# Remove the options while leaving the remaining arguments
shift "$(( OPTIND -1 ))"

# If no argument provided, show help.
if [[ "${#}" -lt 1 ]]; then
  show_usage
  exit 1
fi

# Anything that remains is the command
COMMAND="${@}"

# Make sure server_LIST exists.
if [[ ! -e ${SERVER_LIST} ]]; then
  echo "Cannot open ${SERVER_LIST}." >&2
  exit 1
fi

# Expect the best, prepare for the worst.
EXIT_STATUS='0'

# Loop over the servers.
for SERVER in $(cat servers); do
  if [[ "${VERBOSE}" = 'true' ]]; then
    echo "${SERVER}"
  fi

  SSH_COMMAND="ssh ${SSH_OPTIONS} ${SERVER} ${SUDO} ${COMMAND}"

  # If its a dry run, don't execute aynthing. Just echo it.
  if [[ "${DRY_RUN}" = 'true' ]]; then
    echo "DRY RUN: ${SSH_COMMAND}"
  else
    ${SSH_COMMAND}
    SSH_EXIT_STATUS="${?}"

    # Capture any non-zero exit status from SSH_COMMAND and report to user.
    if [[ "${SSH_EXIT_STATUS}" -ne 0 ]]; then
      echo "Execution on ${SERVER} failed"
      EXIT_STATUS='1'
    fi
  fi
done

exit ${EXIT_STATUS}
