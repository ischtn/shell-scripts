#!/bin/bash

# This script demonstrates I/O redirection.

# Redirect STDOUT to a file
FILE="/tmp/data"
head -n 1 /etc/passwd > ${FILE}

# Redirect STDIN to a program
read LINE < ${FILE}

echo "Line contains: ${LINE}"

# Redirect STOUT to a file, overwriting the file
head -n 3 /etc/passwd > ${FILE}
echo
echo "Contents of ${FILE}:"

# Redirect STOUT to a file appending to the file.
echo "${RANDOM}${RANDOM}" >> ${FILE}
echo "${RANDOM}${RANDOM}" >> ${FILE}
cat ${FILE}

# Redirect STIN to a program, using FD0
read LINE 0< ${FILE}

echo
echo "LINE contains ${LINE}"

# Redirect STDOUT to a file using FD1, overwriting the file
head -n3 /etc/passwd 1> ${FILE}
echo
echo "Contents of ${FILE}"
cat ${FILE}

# Redirect STDERR to STDOUT file
ERR_FILE="/tmp/data.err"
head -n3 /etc/passwd /fakefile 2> ${ERR_FILE}
# head -n /etc/passwd /etc/hosts /fakefile > head.both 2>&1

# newer syntax (with append: >> )
# head -n /etc/passwd /etc/hosts /fakefile &>> head.both


# Another trick:
# head -n1 |& includes STDERR to pipe command.

# Echo something to STDERR
echo "THIS is STDERR" >&2

# Discard STDOUT
echo
echo "Discarding STDOUT:"
head -n3 /etc/passwd /fakefile > /dev/null

echo
echo "Discarding STDERR:"
head -n3 /etc/passwd /fakefile 2> /dev/null

echo
echo "Discarding BOTH:"
head -n3 /etc/passwd /fakefile &> /dev/null

# Clean up
rm ${FILE} ${ERR_FILE} &> /dev/null
